from django.urls import path
from .views import alert_create

urlpatterns = [
    path("create/", alert_create, name="userprofile_create"),
    ]
